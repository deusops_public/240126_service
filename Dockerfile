FROM ubuntu:latest as builder
RUN apt-get update && apt-get install -y curl
RUN curl -fsSL https://deb.nodesource.com/setup_18.x | apt-get install -y nodejs git npm
RUN git clone https://gitfront.io/r/deusops/JnacRhR4iD8q/2048-game.git
WORKDIR 2048-game
RUN npm install --include=dev
RUN npm run build
RUN rm -rf 2048-game/assets && rm package-lock.json

FROM alpine:3.14
RUN apk add --update npm
COPY --from=builder 2048-game .

WORKDIR /2048-game

EXPOSE 8080
CMD [ "npm", "start"]
